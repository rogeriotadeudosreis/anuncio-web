import { TipoEmpreendimentoService } from './../../services/tipo-empreendimento.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoEmpreendimento } from './../../models/tipoEmpreendimento';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipo-empreendimento-delete',
  templateUrl: './tipo-empreendimento-delete.component.html',
  styleUrls: ['./tipo-empreendimento-delete.component.css'],
})
export class TipoEmpreendimentoDeleteComponent implements OnInit {
  nome = new FormControl('', [Validators.required]);

  tipoEmpreendimento: TipoEmpreendimento = {
    nome: '',
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tipoService: TipoEmpreendimentoService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.tipoService.readById(id!).subscribe((resposta) => {
      this.tipoEmpreendimento = resposta;
    });
  }

  deleteTipoEmpreendimento(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.tipoService.delete(id!).subscribe(() => {
      this.router.navigate(['/all-tipo-empreendimento']);
    });
  }

  cancel(): void {
    this.router.navigate(['/all-tipo-empreendimento']);
  }
}

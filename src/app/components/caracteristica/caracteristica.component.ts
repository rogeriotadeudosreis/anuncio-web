import { NotifierService } from 'angular-notifier';
import { Caracteristica } from './../../models/caracteristica';
import { GrupoCaracteristicaService } from './../../services/grupo-caracteristica.service';
import { CaracteristicaService } from './../../services/caracteristica.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GrupoCaracteristica } from 'src/app/models/grupoCaracteristica';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-caracteristica',
  templateUrl: './caracteristica.component.html',
  styleUrls: ['./caracteristica.component.css'],
})
export class CaracteristicaComponent implements OnInit {
  listaGrupoCaracteristica: GrupoCaracteristica[] = [];

  listaCaracteristica: Caracteristica[] = [];

  grupoid = new FormControl('', [Validators.required]);

  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  grupoCaracteristica: GrupoCaracteristica = {
    id: '',
    nome: '',
  };

  caracteristica: Caracteristica = {
    nome: '',
    grupoCaracteristica: this.grupoCaracteristica,
  };

  constructor(
    private router: Router,
    private service: CaracteristicaService,
    private grupoService: GrupoCaracteristicaService,
    private notifier: NotifierService
  ) {}

  ngOnInit(): void {
    this.getAllGrupoCaracteristica();
  }

  createCaracteristica(): void {
    console.log('enviando caracteristica nova....', this.caracteristica);
    this.service.create(this.caracteristica).subscribe(
      () => {
        this.router.navigate(['all-caracteristica']);
        console.log('Cadastro realizado com sucesso!');
      },
      (e) => {
        if (e.error.mensagemUsuario != '') {
          alert(e.error.mensagemUsuario);
        }
        console.log(e.error);
      }
    );
  }

  getAllCaracteristicas(): void {
    this.router.navigate(['all-caracteristica']);
    this.service.listarCaracteristicas().subscribe((resposta) => {
      this.listaCaracteristica = resposta;
    });
  }

  getAllGrupoCaracteristica(): void {
    this.grupoService.listarGrupoCaracteristica().subscribe((resposta) => {
      this.listaGrupoCaracteristica = resposta;
    });
  }
}

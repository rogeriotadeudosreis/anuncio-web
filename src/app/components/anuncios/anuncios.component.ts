import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Anuncio } from 'src/app/models/anuncio';
import { AnuncioService } from 'src/app/services/anuncio.service';

@Component({
  selector: 'app-anuncios',
  templateUrl: './anuncios.component.html',
  styleUrls: ['./anuncios.component.css']
})
export class AnunciosComponent implements OnInit {
  anuncios: Anuncio[] = [];

  constructor(private router: Router, private service: AnuncioService) {}

  ngOnInit(): void {
    this.getAnuncios();
  }

  navegarParaCreateAnuncio(): void {
    console.log('navegando para create-anuncio');
    this.router.navigate(['/create-anuncio']);
  }

  getAnuncios() {
    this.service.get().subscribe((resposta) => {
      this.anuncios = resposta;
      console.log(this.anuncios);
    });
  }

}

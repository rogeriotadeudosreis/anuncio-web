import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-endereco',
  templateUrl: './endereco.component.html',
  styleUrls: ['./endereco.component.css']
})
export class EnderecoComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  navegarParaHome(): void {
    console.log('navegando para create-anuncio');
    this.router.navigate(['/home']);
  }

}

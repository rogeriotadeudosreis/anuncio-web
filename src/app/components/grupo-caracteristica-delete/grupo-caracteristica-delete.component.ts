import { GrupoCaracteristica } from './../../models/grupoCaracteristica';
import { GrupoCaracteristicaService } from './../../services/grupo-caracteristica.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grupo-caracteristica-delete',
  templateUrl: './grupo-caracteristica-delete.component.html',
  styleUrls: ['./grupo-caracteristica-delete.component.css'],
})
export class GrupoCaracteristicaDeleteComponent implements OnInit {
  nome = new FormControl('', [Validators.required]);

  grupo: GrupoCaracteristica = {
    id:'',
    nome: '',
  };

  constructor(
    private router: Router,
    private grupoService: GrupoCaracteristicaService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.grupoService.readById(id!).subscribe((resposta) => {
      this.grupo = resposta;
    });
  }

  deleteGrupoCaracteristica(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.grupoService.delete(id!).subscribe(() => {
      this.router.navigate(['/all-grupo-caracteristica']);
    });
  }

  cancel(): void {
    this.router.navigate(['/all-grupo-caracteristica']);
  }
}

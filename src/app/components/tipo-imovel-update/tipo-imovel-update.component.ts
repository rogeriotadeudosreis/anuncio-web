import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { TipoImovelService } from 'src/app/services/tipo-imovel.service';
import { TipoImovel } from 'src/app/models/tipoImovel';

@Component({
  selector: 'app-tipo-imovel-update',
  templateUrl: './tipo-imovel-update.component.html',
  styleUrls: ['./tipo-imovel-update.component.css'],
})
export class TipoImovelUpdateComponent implements OnInit {
  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  tipoImovel: TipoImovel = {
    nome: '',
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tipoService: TipoImovelService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.tipoService.readById(id!).subscribe((resposta) => {
      this.tipoImovel = resposta;
    });
  }

  updateTipoImovel(): void {
    this.tipoService.update(this.tipoImovel).subscribe(() => {
      this.router.navigate(['/all-tipo-imovel']);
    });
  }

  cancel(): void {
    this.router.navigate(['/all-tipo-imovel']);
  }
}

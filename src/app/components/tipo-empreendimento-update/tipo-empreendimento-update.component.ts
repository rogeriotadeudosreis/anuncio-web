import { TipoEmpreendimento } from './../../models/tipoEmpreendimento';
import { TipoEmpreendimentoService } from './../../services/tipo-empreendimento.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipo-empreendimento-update',
  templateUrl: './tipo-empreendimento-update.component.html',
  styleUrls: ['./tipo-empreendimento-update.component.css'],
})
export class TipoEmpreendimentoUpdateComponent implements OnInit {
  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(2),
    Validators.maxLength(50),
  ]);

  tipoEmpreendimento: TipoEmpreendimento = {
    nome: '',
  };

  constructor(
    private router: Router,
    private tipoService: TipoEmpreendimentoService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.tipoService.readById(id!).subscribe((resposta) => {
      this.tipoEmpreendimento = resposta;
    });
  }

  updateTipoEmpreendimento(): void {
    this.tipoService.update(this.tipoEmpreendimento).subscribe(() => {
      this.router.navigate(['/all-tipo-empreendimento']);
    });
  }

  cancel(): void {
    this.router.navigate(['/all-tipo-empreendimento']);
  }
}

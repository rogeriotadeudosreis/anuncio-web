import { ImagemService } from './../../services/imagem.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-imagem',
  templateUrl: './imagem.component.html',
  styleUrls: ['./imagem.component.css']
})
export class ImagemComponent implements OnInit {
  closed = 0;

  constructor(private router:Router, private imagemService: ImagemService) { }

  ngOnInit(): void {
  }

  listarImagens():void {
    this.imagemService.listarImagens().subscribe((imagens: any[]) => {
      console.log(imagens);
    })
  }

}

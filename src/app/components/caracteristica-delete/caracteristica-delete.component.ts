import { GrupoCaracteristicaService } from './../../services/grupo-caracteristica.service';
import { GrupoCaracteristica } from 'src/app/models/grupoCaracteristica';
import { CaracteristicaService } from 'src/app/services/caracteristica.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Caracteristica } from 'src/app/models/caracteristica';

@Component({
  selector: 'app-caracteristica-delete',
  templateUrl: './caracteristica-delete.component.html',
  styleUrls: ['./caracteristica-delete.component.css'],
})
export class CaracteristicaDeleteComponent implements OnInit {
  listaGrupoCaracteristica: GrupoCaracteristica[] = [];

  grupoid = new FormControl('', [Validators.required]);

  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  grupoCaracteristica: GrupoCaracteristica = {
    id:'',
    nome: '',
  };

  caracteristica: Caracteristica = {
    nome:'',
    grupoCaracteristica: this.grupoCaracteristica,
  };

  constructor(
    private router: Router,
    private service: CaracteristicaService,
    private route: ActivatedRoute,
    private grupoService: GrupoCaracteristicaService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.readById(id!).subscribe((resposta) => {
      this.caracteristica = resposta;
      this.getAllGrupoCaracteristica();
    });
  }

  getAllGrupoCaracteristica(): void {
    this.grupoService.listarGrupoCaracteristica().subscribe((resposta) => {
      this.listaGrupoCaracteristica = resposta;
    });
  }

  deleteCaracteristica(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.delete(id!).subscribe(() => {
      this.router.navigate(['/all-caracteristica']);
    });
  }

  cancel(): void {
    this.router.navigate(['/all-caracteristica']);
  }
}

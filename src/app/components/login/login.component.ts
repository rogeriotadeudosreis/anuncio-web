import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;

  cadastroForm!: FormGroup;
  usuario: Usuario = {
    nome: '',
    email: '',
    senha: '',
    telefone: '',
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: LoginService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['',[ Validators.required, Validators.minLength(3)]],
      senha:['', [ Validators.required, Validators.minLength(6)]],
    })
  }

  
  get email() { return this.loginForm.get('email')! }

  get senha() { return this.loginForm.get('senha')! }

  navegarParaAnuncio():void {
    this.router.navigate(['all-anuncio']);
  }

  login() {
    const email = this.email.value;
    const senha = this.senha.value;

    this.usuario.email = this.email.value;
    this.usuario.senha = this.senha.value;

    this.service.efetuarLogin(this.usuario).subscribe(
      () => {
        this.router.navigate(['/home']);
        console.log('Login realizado com sucesso');
      },
      (e) => {

        if (e.error.mensagemUsuario != '') {
          alert(e.error.mensagemUsuario);
          this.loginForm.reset();
        }
        console.log(e.error)
      }
    );
    
  }
}

import { DadosComplementaresService } from './../../services/dados-complementares.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dados-complementares',
  templateUrl: './dados-complementares.component.html',
  styleUrls: ['./dados-complementares.component.css'],
})
export class DadosComplementaresComponent implements OnInit {
  constructor(
    private router: Router,
    private service: DadosComplementaresService
  ) {}

  ngOnInit(): void {}

  listarDadosComplementares(): void {
    this.service.listarDadosComplementares().subscribe((dados: any[]) => {
      console.log(dados);
    });
  }
}

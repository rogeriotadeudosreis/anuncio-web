import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';
import { CadastroService } from 'src/app/services/cadastro.service';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  cadastroForm!: FormGroup;
  usuario: Usuario = {
    nome: '',
    email: '',
    senha: '',
    telefone: '',
  };

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: CadastroService) { }

    ngOnInit(): void {
      this.cadastroForm = this.formBuilder.group({
        nome: ['', Validators.required ],
        email: ['', Validators.required],
        senha: ['',[ Validators.required, Validators.minLength(6)]],
        confirmaSenha: ['',[ Validators.required, Validators.minLength(6)]],
        celular: ['',Validators.required],
      })
    }


    get nome() { return this.cadastroForm.get('nome')! }

    get email() { return this.cadastroForm.get('email')! }

    get senha() { return this.cadastroForm.get('senha')! }

    get confirmaSenha() { return this.cadastroForm.get('confirmaSenha')! }

    get celular() { return this.cadastroForm.get('celular')! }
    
    cadastrar(){
      const senha = this.senha.value;
      const confirmaSenha = this.confirmaSenha.value;

      if (senha != confirmaSenha) {
        alert("Senhas não conferem");

        this.cadastroForm.get('senha')?.reset();
        this.cadastroForm.get('confirmaSenha')?.reset();
        return;
      }
            
      this.usuario.nome = this.nome.value;
      this.usuario.email = this.email.value;
      this.usuario.senha = this.senha.value;
      this.usuario.telefone = this.celular.value;

      this.service.create(this.usuario).subscribe(
        () => {
          this.router.navigate(['cadastro-sucesso']);
          console.log('Cadastro realizado com sucesso');
        },
        (e) => {

          if (e.error.mensagemUsuario != '') {
            alert(e.error.mensagemUsuario);
            this.cadastroForm.reset();
          }
          console.log(e.error)
        }
      );
  }

}

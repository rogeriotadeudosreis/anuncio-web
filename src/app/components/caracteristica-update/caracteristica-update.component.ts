import { GrupoCaracteristicaService } from './../../services/grupo-caracteristica.service';
import { Observable } from 'rxjs';
import { GrupoCaracteristica } from 'src/app/models/grupoCaracteristica';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CaracteristicaService } from 'src/app/services/caracteristica.service';
import { Caracteristica } from 'src/app/models/caracteristica';

@Component({
  selector: 'app-caracteristica-update',
  templateUrl: './caracteristica-update.component.html',
  styleUrls: ['./caracteristica-update.component.css'],
})
export class CaracteristicaUpdateComponent implements OnInit {
  listaGrupoCaracteristica: GrupoCaracteristica[] = [];

  grupoid = new FormControl();
  
  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  grupoCaracteristica: GrupoCaracteristica = {
    id:'',
    nome: ''
  };

  caracteristica: Caracteristica = {
    nome:'',
    grupoCaracteristica: this.grupoCaracteristica,
  };

  constructor(
    private router: Router,
    private caracteristicaService: CaracteristicaService,
    private route: ActivatedRoute,
    private grupoService: GrupoCaracteristicaService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.caracteristicaService.readById(id!).subscribe((resposta) => {
      this.caracteristica = resposta;
      this.grupoid.setValue(this.caracteristica.grupoCaracteristica);
      this.getAllGrupoCaracteristica();
      console.log(this.caracteristica)
    });
  }

  updateCaracteristica(): void {
    console.log(this.caracteristica.grupoCaracteristica);
    this.caracteristicaService.update(this.caracteristica).subscribe(() => {
      this.router.navigate(['/all-caracteristica']);
    });
  }

  getAllGrupoCaracteristica(): void {
    this.grupoService.listarGrupoCaracteristica().subscribe((resposta) => {
      this.listaGrupoCaracteristica = resposta;
      console.log(this.listaGrupoCaracteristica);
    });
  }

  cancel(): void {
    this.router.navigate(['/all-caracteristica']);
  }



}

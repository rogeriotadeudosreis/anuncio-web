import { FormControl, Validators } from '@angular/forms';
import { TipoEmpreendimentoService } from './../../services/tipo-empreendimento.service';
import { TipoEmpreendimento } from './../../models/tipoEmpreendimento';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-tipo-empreendimento',
  templateUrl: './all-tipo-empreendimento.component.html',
  styleUrls: ['./all-tipo-empreendimento.component.css'],
})
export class AllTipoEmpreendimentoComponent implements OnInit {
  listaTipoEmpreendimento: TipoEmpreendimento[] = [];

  nomeForm = new FormControl('', [Validators.required]);
  nome = '';

  constructor(
    private router: Router,
    private tiposervice: TipoEmpreendimentoService
  ) {}

  ngOnInit(): void {
    this.getAllTipoEmpreendimento();
  }

  getAllTipoEmpreendimento(): void {
    this.router.navigate(['all-tipo-empreendimento']);
    this.tiposervice.listarTipoEmpreendimento().subscribe((resposta) => {
      this.listaTipoEmpreendimento = resposta;
      this.nome = '';
      console.log(this.listaTipoEmpreendimento);
    });
  }

  getByName(): void {
    this.tiposervice.readByName(this.nome).subscribe((resposta) => {
      this.listaTipoEmpreendimento = resposta;
    })
  }

  public navegarParaTipoEmpreendimento(): void {
    this.router.navigate(['tipo-empreendimento']);
  }
}

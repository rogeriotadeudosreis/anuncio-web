import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Anuncio } from 'src/app/models/anuncio';
import { Caracteristica } from 'src/app/models/caracteristica';
import { DadosComplementares } from 'src/app/models/dadosComplementares';
import { Endereco } from 'src/app/models/endereco';
import { GrupoCaracteristica } from 'src/app/models/grupoCaracteristica';
import { Imagem } from 'src/app/models/imagem';
import { TipoEmpreendimento } from 'src/app/models/tipoEmpreendimento';
import { TipoImovel } from 'src/app/models/tipoImovel';
import { Usuario } from 'src/app/models/usuario';
import { AnuncioService } from 'src/app/services/anuncio.service';
import { CaracteristicaService } from 'src/app/services/caracteristica.service';
import { EnderecoService } from 'src/app/services/endereco.service';
import { ImagemService } from 'src/app/services/imagem.service';
import { TipoEmpreendimentoService } from 'src/app/services/tipo-empreendimento.service';
import { TipoImovelService } from 'src/app/services/tipo-imovel.service';

@Component({
  selector: 'app-anuncio-delete',
  templateUrl: './anuncio-delete.component.html',
  styleUrls: ['./anuncio-delete.component.css']
})
export class AnuncioDeleteComponent implements OnInit {

  constructor(
    private router: Router,
    private anuncioService: AnuncioService,
    private imovelService: TipoImovelService,
    private empreendimentoService: TipoEmpreendimentoService,
    private enderecoService: EnderecoService,
    private caracteristicaService: CaracteristicaService, 
    private httpClient: HttpClient,
    private imagemService: ImagemService,
    private route: ActivatedRoute,
  ) {}

  listaTipoImovel: TipoImovel[] = [];

  listaTipoEmpreendimento: TipoEmpreendimento[] = [];

  listaAnuncio: Anuncio[] = [];

  listaCaracteristica:any = [];

  listaAnuncioCaracteristica: any = [];

  selectedValues: string[] = [];

  listaImagem:Imagem[] = [];

  grupoCaracteristica: GrupoCaracteristica = {
    nome:'',
  }

  caracteristica: Caracteristica = {
    nome: '',
    grupoCaracteristica: this.grupoCaracteristica,
  }

  endereco: Endereco = {
    cep: '',
    logradouro: '',
    complemento: '',
    bairro: '',
    localidade: '',
    uf: '',
    ibge: '',
    gia: '',
    ddd: '',
    siafi: '',
    numero: '',
  };

  enderecocep = new FormControl('', [Validators.required, Validators.minLength(9), Validators.maxLength(9)]);
  enderecologradouro = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]);
  enderecocomplemento = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]);
  enderecobairro = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]);
  enderecolocalidade = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(100)]);
  enderecouf = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]);
  endereconumero = new FormControl('', [Validators.required,Validators.minLength(1), Validators.maxLength(6)]);

  tipoImovel: TipoImovel = {
    nome: '',
  };
  tipoimovelnome = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)],);

  tipoEmpreendimento: TipoEmpreendimento = {
    nome: '',
  };
  tipoempreendimentonome = new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]);

  dadosComplementares: DadosComplementares = {
  nomeEmpreendimento:'',
  valor: '',
  metrosQuadrados: '',
  qtdQuartos: '',
  qtdSuites: '',
  qtdBanheiros: '',
  qtdVagas: '',
  }

  dadosnomeempreendimento = new FormControl('',[Validators.required, Validators.minLength(3), Validators.maxLength(50)]);
  dadosvalor = new FormControl('',[Validators.required]);
  dadosmetrosquadrados = new FormControl('',[Validators.required]);
  dadosqtdquartos = new FormControl('',[Validators.required]);
  dadosqtdsuites = new FormControl('',[Validators.required]);
  dadosqtdbanheiros = new FormControl('',[Validators.required]);
  dadosqtdvagas = new FormControl('',[Validators.required]);

  usuario: Usuario = {

    nome: '',
    email: '',
    senha: '',
    telefone: '',
    perfil: 'ADMINISTRADOR',
    dtCadastro: '',  
    dtAtualizacao: '',   
    ativo: '',
  }

  usuarioTest = [
    {label:'Aires Ribeiro', value:'1'},
    {label:'Gabriel Cunha', value:'2'},
    {label:'Lucas França', value:'3'},
    {label:'Rogério Reis', value:'4'},
  ]

  estados = [
    { label: 'Acre', value: 'AC' },
    { label: 'Alagoas', value: 'AL' },
    { label: 'Amapá', value: 'AP' },
    { label: 'Amazonas', value: 'AM' },
    { label: 'Bahia', value: 'BA' },
    { label: 'Ceará', value: 'CE' },
    { label: 'Distrito Federal', value: 'DF' },
    { label: 'Espírito Santo', value: 'ES' },
    { label: 'Goiás', value: 'GO' },
    { label: 'Maranhão', value: 'MA' },
    { label: 'Mato Grosso', value: 'MT' },
    { label: 'Matro Grosso do Sul', value: 'MS' },
    { label: 'Minas Gerais', value: 'MG' },
    { label: 'Pará', value: 'PA' },
    { label: 'Paraíba', value: 'PB' },
    { label: 'Paraná', value: 'PR' },
    { label: 'Pernambuco', value: 'PE' },
    { label: 'Piauí', value: 'PI' },
    { label: 'Rio de Janeiro', value: 'RJ' },
    { label: 'Rio Grande do Norte', value: 'RN' },
    { label: 'Rio Grande do Sul', value: 'RS' },
    { label: 'Rondônia', value: 'RO' },
    { label: 'Roraima', value: 'RR' },
    { label: 'Santa Catarina', value: 'SC' },
    { label: 'São Paulo', value: 'SP' },
    { label: 'Sergipe', value: 'SE' },
    { label: 'Tocantins', value: 'TO' },
  ];

  contador = [
    {label:'00', value:'0'},
    {label:'01', value:'1'},
    {label:'02', value:'2'},
    {label:'03', value:'3'},
    {label:'04', value:'4'},
    {label:'05', value:'5'},
    {label:'06', value:'6'},
    {label:'07', value:'7'},
    {label:'08', value:'8'},
    {label:'09', value:'9'},
    {label:'10', value:'10'},
  ];

  anuncio: Anuncio = {
  id:'',
  ativo: 'true',
  caracteristicas: this.listaAnuncioCaracteristica,
  categoria: '', 
  dadosComplementares: this.dadosComplementares,
  descricao: '',
  dataCadastro:'',
  endereco: this.endereco,
  imagem: [],
  qtdDeVisualizacoes: '', 
  tipoEmpreendimento: this.tipoEmpreendimento,
  tipoImovel: this.tipoImovel,
  usuario: this.usuario,
  }

  imagem: Imagem = {
   id:'',
   imagem:''
  }

  anunciocategoria = new FormControl('',[Validators.required])
  anunciodescricao = new FormControl('',[Validators.required])
  anuncioativo = new FormControl('',[Validators.required])
  anunciodtdecadastro = new FormControl('',[Validators.required])

  ngOnInit(): void {

    const id = this.route.snapshot.paramMap.get('id');
    this.anuncioService.readById(id!).subscribe((resposta) => {
      this.anuncio = resposta;
      console.log('Exibindo dados do anúncio...', this.anuncio);
      console.log(
        'Exibindo somente as características...',
        this.anuncio.caracteristicas
      );
    });
    this.getAllTipoImovel();
    this.getAllTipoEmpreendimento();
    this.getAllCaracteristica();

    function adicionaZero(numero: string | number){
      if (numero <= 9) 
          return "0" + numero;
      else
          return numero; 
  }
  var dataAtual = new Date(); 
  var dataAtualFormatada = (dataAtual.getFullYear()  + "-"
   + (adicionaZero(dataAtual.getMonth()+1).toString()) + "-"
   + adicionaZero(dataAtual.getDate().toString()));
  console.log(dataAtualFormatada);
  this.anuncio.dataCadastro = dataAtualFormatada;
  this.usuario.dtAtualizacao = dataAtualFormatada;
  }

  deletarAnuncio(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.anuncioService.delete(id!).subscribe(() => {
      this.router.navigate(['/all-anuncio']);
    });
  }


  listaDeAnunciosAdmin(): void {
    this.router.navigate(['/all-anuncio']);
  }

  temLista(): boolean {
    var listPresent = true;
    if (this.anuncio.caracteristicas.length == 0) {
      listPresent = false;
    }
    return listPresent;
  }
  
  updateAnuncio(): void {
    console.log('Enviando dados do anúncio...',this.anuncio)
    this.enviarCaracteristica();
    this.anuncioService.update(this.anuncio).subscribe(() => {
      this.listaDeAnunciosAdmin();
    }, (e) => {
      if (e.error.mensagemUsuario != '') {
        alert(e.error.mensagemUsuario);
      }
    });
  }

  imagemListener(evt: any){
    var file:File = evt.target.files[0];
    var myReader:FileReader = new FileReader();  

    myReader.onloadend = (e) => {
      let img: Imagem = {
        imagem: <string>myReader.result
       }
       this.anuncio.imagem.push(img);
    }
    myReader.readAsDataURL(file); 
  }

  validaQuantFotos():boolean {
    var validado = false;
    if (this.anuncio.imagem.length >= 3) {
      validado = true;
    }
    return validado;
  }

  getAllAnuncios(): void {
    this.router.navigate(['/all-anuncio']);
    this.anuncioService.get().subscribe((resposta) => {
      this.listaAnuncio = resposta;
      console.log(this.listaAnuncio);
    });
  }

  getAllTipoImovel(): void {
    this.imovelService.listarTipoImovel().subscribe((resposta) => {
      this.listaTipoImovel = resposta;
      console.log("Carregando tipos de imovel...",this.listaTipoImovel);
    });
  }

  getAllTipoEmpreendimento(): void {
    this.empreendimentoService
      .listarTipoEmpreendimento()
      .subscribe((resposta) => {
        this.listaTipoEmpreendimento = resposta;
        console.log('Carregando tipos de empreendimento...',this.listaTipoEmpreendimento);
      });
  }

  getEnderecoPeloCep(cep: string) {
    this.enderecoService.getEnderecoCep(cep).subscribe((resposta) => {
      this.endereco.logradouro = resposta.logradouro;
      this.endereco.complemento = resposta.complemento;
      this.endereco.bairro = resposta.bairro;
      this.endereco.localidade = resposta.localidade;
      this.endereco.numero = resposta.numero;
      this.endereco.uf = resposta.uf;
      this.endereco.ddd = resposta.ddd;
      this.endereco.gia = resposta.gia;
      this.endereco.ibge = resposta.ibge;
      this.endereco.siafi = resposta.siafi;
      console.log('Buscando endereço via cep...',this.endereco);
    });
  }

  enviarCaracteristica(){
    for(let r of this.listaCaracteristica){
      if(r.select){
        this.listaAnuncioCaracteristica.push(r.caract);
      }      
    }
    console.log('Carregando características...',this.listaAnuncioCaracteristica);
  }

  onChangeListaCaracteristica(item:any, event:any){
    item.select = event.target.checked;
  }

  getAllCaracteristica(): void {
    this.caracteristicaService.listarCaracteristicas().subscribe((resposta) => {
      this.listaCaracteristica = [];
      for(let r of resposta){
        let c = {select: false, caract:r};
        this.listaCaracteristica.push(c);
      }
    })
  }

}

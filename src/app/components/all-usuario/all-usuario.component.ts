import { UsuarioService } from './../../services/usuario.service';
import { Usuario } from './../../models/usuario';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-usuario',
  templateUrl: './all-usuario.component.html',
  styleUrls: ['./all-usuario.component.css'],
})
export class AllUsuarioComponent implements OnInit {
  listaUsuario: Usuario[] = [];

  constructor(private router: Router, private usuarioService: UsuarioService) {}

  ngOnInit(): void {
    this.getAllUsuario();
  }

  getAllUsuario(): void {
    this.router.navigate(['all-usuario']);
    this.usuarioService.listarUsuarios().subscribe((resposta) => {
      this.listaUsuario = resposta;
    });
  }

  public navegarParaUsuario(): void {
    this.router.navigate(['cadastro']);
  }
}

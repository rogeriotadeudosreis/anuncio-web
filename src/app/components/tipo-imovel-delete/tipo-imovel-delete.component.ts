import { TipoImovelService } from 'src/app/services/tipo-imovel.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { TipoImovel } from 'src/app/models/tipoImovel';

@Component({
  selector: 'app-tipo-imovel-delete',
  templateUrl: './tipo-imovel-delete.component.html',
  styleUrls: ['./tipo-imovel-delete.component.css'],
})
export class TipoImovelDeleteComponent implements OnInit {
  nome = new FormControl('', [Validators.required]);

  tipoImovel: TipoImovel = {
    nome: '',
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private tipoService: TipoImovelService
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.tipoService.readById(id!).subscribe((resposta) => {
      this.tipoImovel = resposta;
    });
  }

  deleteTipoImovel(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.tipoService.delete(id!).subscribe(() => {
      this.router.navigate(['/all-tipo-imovel']);
    });
  }

  cancel():void{
    this.router.navigate(['/all-tipo-imovel'])
  }
}

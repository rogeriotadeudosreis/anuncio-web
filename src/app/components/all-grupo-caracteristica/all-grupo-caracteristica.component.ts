import { Validators, FormControl } from '@angular/forms';
import { GrupoCaracteristicaService } from './../../services/grupo-caracteristica.service';
import { GrupoCaracteristica } from 'src/app/models/grupoCaracteristica';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-grupo-caracteristica',
  templateUrl: './all-grupo-caracteristica.component.html',
  styleUrls: ['./all-grupo-caracteristica.component.css'],
})
export class AllGrupoCaracteristicaComponent implements OnInit {
  listaGrupoCaracteristica: GrupoCaracteristica[] = [];

  nomeForm = new FormControl('', [Validators.required]);

  nome: string = '';

  constructor(
    private router: Router,
    private grupoService: GrupoCaracteristicaService
  ) {}

  ngOnInit(): void {
    this.getAllGrupoCaracteristica();
  }

  getAllGrupoCaracteristica(): void {
    this.grupoService.listarGrupoCaracteristica().subscribe((resposta) => {
      this.listaGrupoCaracteristica = resposta;
      this.nome = '';
      console.log(resposta);
    });
  }

  createGrupoCaracteristica(): void {
    this.router.navigate(['/grupo-caracteristica']);
  }

  findByName(): void {
    this.grupoService.readByName(this.nome).subscribe((resposta) => {
      this.listaGrupoCaracteristica = resposta;
      this.router.navigate(['/all-grupo-caracteristica']);
    });
  }
}

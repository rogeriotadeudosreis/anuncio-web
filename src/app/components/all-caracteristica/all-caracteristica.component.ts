import { FormControl, Validators } from '@angular/forms';
import { CaracteristicaService } from './../../services/caracteristica.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Caracteristica } from 'src/app/models/caracteristica';

@Component({
  selector: 'app-all-caracteristica',
  templateUrl: './all-caracteristica.component.html',
  styleUrls: ['./all-caracteristica.component.css'],
})
export class AllCaracteristicaComponent implements OnInit {
  listaCaracteristica: Caracteristica[] = [];

  nomeForm = new FormControl('', [Validators.required]);

  nome: string = '';

  constructor(
    private router: Router,
    private caracteristicaService: CaracteristicaService
  ) {}

  ngOnInit(): void {
    this.getAllCaracteristica();
  }

  getAllCaracteristica(): void {
    this.caracteristicaService.listarCaracteristicas().subscribe((resposta) => {
      this.listaCaracteristica = resposta;
      this.nome = ''; 
      console.log(this.listaCaracteristica);
    });
  }

  navegarParaCaracteristica(): void {
    this.router.navigate(['/caracteristica']);
  }

  findByName(): void {
    this.caracteristicaService.readByName(this.nome).subscribe((resposta) => {
      this.listaCaracteristica = resposta;
    });
  }
}

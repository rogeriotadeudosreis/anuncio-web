import { Router } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private router:Router) {}

  ngOnInit(): void {
    this.navegarParaHome()
  }
  
  public navegarParaHome(): void {
    this.router.navigate(['home'])
  }
}


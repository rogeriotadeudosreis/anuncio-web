import { FormControl, Validators } from '@angular/forms';
import { TipoEmpreendimento } from './../../models/tipoEmpreendimento';
import { TipoEmpreendimentoService } from './../../services/tipo-empreendimento.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipo-empreendimento',
  templateUrl: './tipo-empreendimento.component.html',
  styleUrls: ['./tipo-empreendimento.component.css'],
})
export class TipoEmpreendimentoComponent implements OnInit {
  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  tipo: TipoEmpreendimento[] = [];

  tipoEmpreendimento: TipoEmpreendimento = {
    nome: '',
  };

  constructor(
    private router: Router,
    private service: TipoEmpreendimentoService
  ) {}

  ngOnInit(): void {}

  listarTipoEmpreendimento(): void {
    this.router.navigate(['all-tipo-empreendimento']);
    this.service.listarTipoEmpreendimento().subscribe((resposta) => {
      this.tipo = resposta;
      console.log(this.tipo);
    });
  }

  createTipoEmpreendimento(): void {
    this.service.create(this.tipoEmpreendimento).subscribe(
      () => {
        this.router.navigate(['all-tipo-empreendimento']);
        console.log('Cadastro realizado com sucesso');
      },
      (e) => {

        if (e.error.mensagemUsuario != '') {
          alert(e.error.mensagemUsuario);
        }
        console.log(e.error)
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro-sucesso',
  templateUrl: './cadastro-sucesso.component.html',
  styleUrls: ['./cadastro-sucesso.component.css']
})
export class CadastroSucessoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  redirecionarLogin() {
    this.router.navigate(['login']);
  }

}

import { AnuncioService } from './../../services/anuncio.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Anuncio } from 'src/app/models/anuncio';

@Component({
  selector: 'app-all-anuncio',
  templateUrl: './all-anuncio.component.html',
  styleUrls: ['./all-anuncio.component.css'],
})
export class AllAnuncioComponent implements OnInit {
  listaAnuncio: Anuncio[] = [];

  reais: String = 'R$';

  constructor(private router: Router, private service: AnuncioService) {}

  ngOnInit(): void {
    console.log('Carregando lista de anúncios...',this.getAnuncios());
  }

  navegarParaCreateAnuncio(): void {
    console.log('navegando para create-anuncio');
    this.router.navigate(['/create-anuncio']);
  }

  getAnuncios() {
    this.service.get().subscribe((resposta) => {
      this.listaAnuncio = resposta;
      console.log(this.listaAnuncio);
    });
  }

  deletarAnuncio(){
    this.router.navigate(['/anuncio-delete'])
  }

  
}

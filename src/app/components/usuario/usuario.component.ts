import { UsuarioService } from './../../services/usuario.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
})
export class UsuarioComponent implements OnInit {
  
  constructor(private httpClient: HttpClient, private router: Router, private service: UsuarioService) {}

  ngOnInit(): void {}

  navegarParaHome(): void {
    this.router.navigate(['/home']);
  }

  create(usuario: Usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(
      `${environment.baseUrl}/usuario`, usuario);
  }

  listarUsuarios(): void {

    this.router.navigate(['all-usuario'])
    console.log('Listando usuarios...')
    this.service.listarUsuarios().subscribe((usuarios: any[]) => {
      console.log(usuarios);
    })
  }

}

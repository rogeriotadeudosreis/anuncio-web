import { FormControl, Validators } from '@angular/forms';
import { GrupoCaracteristica } from './../../models/grupoCaracteristica';
import { GrupoCaracteristicaService } from './../../services/grupo-caracteristica.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-grupo-caracteristica-update',
  templateUrl: './grupo-caracteristica-update.component.html',
  styleUrls: ['./grupo-caracteristica-update.component.css'],
})
export class GrupoCaracteristicaUpdateComponent implements OnInit {
  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  grupo: GrupoCaracteristica = {
    id:'',
    nome: '',
  };

  constructor(
    private router: Router,
    private gruposervice: GrupoCaracteristicaService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.gruposervice.readById(id!).subscribe((resposta) => {
      this.grupo = resposta;
    });
  }

  updateGrupoCaracteristica(): void {
    this.gruposervice.update(this.grupo).subscribe(() => {
      this.router.navigate(['/all-grupo-caracteristica']);
    });
  }

  cancel(): void {
    this.router.navigate(['all-grupo-caracteristica']);
  }
}

import { GrupoCaracteristica } from 'src/app/models/grupoCaracteristica';
import { GrupoCaracteristicaService } from './../../services/grupo-caracteristica.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-grupo-caracteristica',
  templateUrl: './grupo-caracteristica.component.html',
  styleUrls: ['./grupo-caracteristica.component.css'],
})
export class GrupoCaracteristicaComponent implements OnInit {
 
  banana = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  listaGrupoCaracteristica: GrupoCaracteristica[] = [];

  grupo: GrupoCaracteristica = {
    id:'',
    nome: '',
  };

  constructor(
    private router: Router,
    private grupoService: GrupoCaracteristicaService
  ) {}

  ngOnInit(): void {}

  listarGrupoCaracteristica(): void {
    this.router.navigate(['all-grupo-caracteristica']);
    this.grupoService.listarGrupoCaracteristica().subscribe((resposta) => {
      this.listaGrupoCaracteristica = resposta;
      console.log(resposta);
    });
  }

  createGrupoCaracteristica(): void {
    this.grupoService.create(this.grupo).subscribe(() => {
        this.router.navigate(['/all-grupo-caracteristica']);
        console.log('Cadastro realizado com sucesso!')
      }, (e) => {

        if (e.error.mensagemUsuario != '') {
          alert(e.error.mensagemUsuario);
        }
        console.log(e.error)
  });
  }
}

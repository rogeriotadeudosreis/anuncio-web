import { TipoImovel } from './../../models/tipoImovel';
import { TipoEmpreendimento } from './../../models/tipoEmpreendimento';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Anuncio } from 'src/app/models/anuncio';
import { AnuncioService } from 'src/app/services/anuncio.service';
import { Caracteristica } from 'src/app/models/caracteristica';
import { Endereco } from 'src/app/models/endereco';
import { DadosComplementares } from 'src/app/models/dadosComplementares';
import { Usuario } from 'src/app/models/usuario';

@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.component.html',
  styleUrls: ['./detalhes.component.css'],
})
export class DetalhesComponent implements OnInit {
  dadosComplementares: DadosComplementares = {
    nomeEmpreendimento: '',
    valor: '',
    metrosQuadrados: '',
    qtdQuartos: '',
    qtdSuites: '',
    qtdBanheiros: '',
    qtdVagas: '',
  };

  endereco: Endereco = {
    cep: '',
    logradouro: '',
    complemento: '',
    bairro: '',
    localidade: '',
    uf: '',
    ibge: '',
    gia: '',
    ddd: '',
    siafi: '',
    numero: '',
  };

  tipoEmpreendimento: TipoEmpreendimento = {
    nome: '',
  };

  tipoImovel: TipoImovel = {
    nome: '',
  };

  usuario: Usuario = {
    nome: '',
    email: '',
    senha: '',
    telefone: '',
    perfil: '',
    dtCadastro: '',
    dtAtualizacao: '',
    ativo: '',
  };

  anuncio: Anuncio = {
    id: '',
    ativo: '',
    caracteristicas: [],
    categoria: '',
    dadosComplementares: this.dadosComplementares,
    descricao: '',
    dataCadastro: '',
    endereco: this.endereco,
    imagem: [],
    qtdDeVisualizacoes: '',
    tipoEmpreendimento: this.tipoEmpreendimento,
    tipoImovel: this.tipoImovel,
    usuario: this.usuario,
  };

  constructor(
    private router: Router,
    private anuncioService: AnuncioService,
    private route: ActivatedRoute
  ) {}

  temLista(): boolean {
    var listPresent = true;
    if (this.anuncio.caracteristicas.length == 0) {
      listPresent = false;
    }
    return listPresent;
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.anuncioService.readById(id!).subscribe((resposta) => {
      this.anuncio = resposta;
      console.log('Exibindo dados do anúncio...', this.anuncio);
      console.log(
        'Exibindo somente as características...',
        this.anuncio.caracteristicas
      );
    });
  }

  // função para exibir a imagem dentro da modal
  showImageWithModal(item: any) {
    return this.anuncio.imagem = item;
  }

  contato() {
    window.open(
      'https://api.whatsapp.com/send?phone=' + this.anuncio.usuario.telefone,
      '_blank'
    );
  }
}

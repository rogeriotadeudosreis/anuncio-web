import { TipoImovelService } from './../../services/tipo-imovel.service';
import { TipoImovel } from './../../models/tipoImovel';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-all-tipo-imovel',
  templateUrl: './all-tipo-imovel.component.html',
  styleUrls: ['./all-tipo-imovel.component.css'],
})
export class AllTipoImovelComponent implements OnInit {
  listaTipoImovel: TipoImovel[] = [];

  constructor(private router: Router, private tipoService: TipoImovelService) {}

  ngOnInit(): void {
    this.getAllTipoImovel();
  }

  getAllTipoImovel(): void {
    this.router.navigate(['all-tipo-imovel']);
    this.tipoService.listarTipoImovel().subscribe((resposta) => {
      this.listaTipoImovel = resposta;
    });
  }

  public navegarParaTipoImovel(): void {
    this.router.navigate(['tipo-imovel']);
  }
}

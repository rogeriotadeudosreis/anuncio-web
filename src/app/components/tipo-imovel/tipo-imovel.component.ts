import { FormControl, Validators } from '@angular/forms';
import { TipoImovel } from './../../models/tipoImovel';
import { TipoImovelService } from './../../services/tipo-imovel.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipo-imovel',
  templateUrl: './tipo-imovel.component.html',
  styleUrls: ['./tipo-imovel.component.css']
})
export class TipoImovelComponent implements OnInit {
  nome = new FormControl('', [
    Validators.required,
    Validators.minLength(3),
    Validators.maxLength(50),
  ]);

  listaDeTipoImovel: TipoImovel[] = [];

  tipoImovel: TipoImovel = {
    nome: ''
  }

  constructor(private router: Router, private service:TipoImovelService) { }

  ngOnInit(): void {
  }

  getAllTipoImovel(): void{
    this.router.navigate(['all-tipo-imovel'])
    this.service.listarTipoImovel().subscribe((resposta) =>{
      this.listaDeTipoImovel = resposta;
      console.log(this.listaDeTipoImovel);
    })
  }

  createTipoImovel(): void {
    this.service.create(this.tipoImovel).subscribe(() => {
      this.router.navigate(['all-tipo-imovel'])
      console.log('Cadastro realizado com sucesso!')
    }, (e) => {

      if (e.error.mensagemUsuario != '') {
        alert(e.error.mensagemUsuario);
      }
      console.log(e.error)
    }); 
  }
}

import { GrupoCaracteristica } from 'src/app/models/grupoCaracteristica';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class GrupoCaracteristicaService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  listarGrupoCaracteristica(): Observable<GrupoCaracteristica[]> {
    return this.httpClient.get<GrupoCaracteristica[]>(
      `${this.baseUrl}/grupocaracteristica`
    );
  }

  create(grupo: GrupoCaracteristica): Observable<GrupoCaracteristica> {
    return this.httpClient.post<GrupoCaracteristica>(
      `${this.baseUrl}/grupocaracteristica`, grupo);
  }

  readByName(nome: String): Observable<GrupoCaracteristica[]> {
    const url = `${this.baseUrl}/grupocaracteristica/filter?name=${nome}`;
    return this.httpClient.get<GrupoCaracteristica[]>(url);
  }

  readById(id: string): Observable<GrupoCaracteristica> {
    const url = `${this.baseUrl}/grupocaracteristica/${id}`;
    return this.httpClient.get<GrupoCaracteristica>(url);
  }

  update(grupo: GrupoCaracteristica): Observable<GrupoCaracteristica> {
    const url = `${this.baseUrl}/grupocaracteristica/${grupo.id}`;
    return this.httpClient.put<GrupoCaracteristica>(url, grupo);
  }

  delete(id: string): Observable<GrupoCaracteristica> {
    const url = `${this.baseUrl}/grupocaracteristica/${id}`;
    return this.httpClient.delete<GrupoCaracteristica>(url);
  }
}

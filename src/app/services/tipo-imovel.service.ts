import { TipoImovel } from './../models/tipoImovel';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TipoImovelService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  listarTipoImovel(): Observable<TipoImovel[]> {
    return this.httpClient.get<TipoImovel[]>(`${this.baseUrl}/tipoimovel`);
  }

  create(tipoImovel: TipoImovel): Observable<TipoImovel> {
    return this.httpClient.post<TipoImovel>(
      `${this.baseUrl}/tipoimovel`,
      tipoImovel
    );
  }

  readById(id: string): Observable<TipoImovel> {
    const url = `${this.baseUrl}/tipoimovel/${id}`;
    return this.httpClient.get<TipoImovel>(url);
  }

  update(tipoImovel: TipoImovel): Observable<TipoImovel> {
    const url = `${this.baseUrl}/tipoimovel/${tipoImovel.id}`;
    return this.httpClient.put<TipoImovel>(url, tipoImovel);
  }

  delete(id: string): Observable<TipoImovel> {
    const url = `${this.baseUrl}/tipoimovel/${id}`;
    return this.httpClient.delete<TipoImovel>(url);
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class EnderecoService {
  baseUrl = environment.baseUrl;

  urlCep = 'http://viacep.com.br/ws/';

  constructor(private httpClient: HttpClient) {}

  getEnderecoCep(cep: string): Observable<any> {
    return this.httpClient.get(this.urlCep + cep + '/json/');
  }
}

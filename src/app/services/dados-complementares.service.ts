import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DadosComplementaresService {

  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) { }

  listarDadosComplementares(): Observable<any[]>{
    return this.httpClient.get<any[]>(`${this.baseUrl}/dadoscomplementares/1`);
  }
}

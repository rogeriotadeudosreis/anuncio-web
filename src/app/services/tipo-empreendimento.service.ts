import { TipoEmpreendimento } from './../models/tipoEmpreendimento';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TipoEmpreendimentoService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  listarTipoEmpreendimento(): Observable<TipoEmpreendimento[]> {
    return this.httpClient.get<TipoEmpreendimento[]>(
      `${this.baseUrl}/tipoempreendimento`
    );
  }

  create(
    tipoEmpreendimento: TipoEmpreendimento
  ): Observable<TipoEmpreendimento> {
    return this.httpClient.post<TipoEmpreendimento>(
      `${this.baseUrl}/tipoempreendimento`,
      tipoEmpreendimento
    );
  }

  readById(id: string): Observable<TipoEmpreendimento> {
    const url = `${this.baseUrl}/tipoempreendimento/${id}`;
    return this.httpClient.get<TipoEmpreendimento>(url);
  }

  update(tipo: TipoEmpreendimento): Observable<TipoEmpreendimento> {
    const url = `${this.baseUrl}/tipoempreendimento/${tipo.id}`;
    return this.httpClient.put<TipoEmpreendimento>(url, tipo);
  }

  delete(id: string): Observable<TipoEmpreendimento> {
    const url = `${this.baseUrl}/tipoempreendimento/${id}`;
    return this.httpClient.delete<TipoEmpreendimento>(url);
  }

  readByName(nome: string): Observable<TipoEmpreendimento[]>{
    const url = `${this.baseUrl}/tipoempreendimento/filter?name=${nome}`;
    return this.httpClient.get<TipoEmpreendimento[]>(url);
  }
}

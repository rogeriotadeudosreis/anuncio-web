import { Caracteristica } from './../models/caracteristica';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CaracteristicaService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  create(caracteristica: Caracteristica): Observable<Caracteristica> {
    return this.httpClient.post<Caracteristica>(
      `${this.baseUrl}/caracteristica`,
      caracteristica
    );
  }

  readByName(nome: string): Observable<Caracteristica[]> {
    const url = `${this.baseUrl}/caracteristica/filter?name=${nome}`;
    return this.httpClient.get<Caracteristica[]>(url);
  }

  readById(id: string): Observable<Caracteristica> {
    const url = `${this.baseUrl}/caracteristica/${id}`;
    return this.httpClient.get<Caracteristica>(url);
  }

  update(caracteristica: Caracteristica): Observable<Caracteristica> {
    const url = `${this.baseUrl}/caracteristica/${caracteristica.id}`;
    return this.httpClient.put<Caracteristica>(url, caracteristica);
  }

  delete(id: string): Observable<Caracteristica> {
    const url = `${this.baseUrl}/caracteristica/${id}`;
    return this.httpClient.delete<Caracteristica>(url);
  }

  listarCaracteristicas(): Observable<Caracteristica[]> {
    return this.httpClient.get<Caracteristica[]>(
      `${this.baseUrl}/caracteristica`
    );
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  efetuarLogin(usuario: Usuario): Observable<Usuario> {
    return this.httpClient.post<Usuario>(
      `${this.baseUrl}/usuario/login`,
      usuario
    );
  }



}

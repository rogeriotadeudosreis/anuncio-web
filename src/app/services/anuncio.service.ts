import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Anuncio } from '../models/anuncio';

@Injectable({
  providedIn: 'root',
})
export class AnuncioService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  create(anuncio: Anuncio): Observable<Anuncio> {
    return this.httpClient.post<Anuncio>(`${this.baseUrl}/anuncio`, anuncio);
  }

  readById(id: string): Observable<Anuncio> {
    const url = `${this.baseUrl}/anuncio/${id}`;
    return this.httpClient.get<Anuncio>(url);
  }

  update(anuncio: Anuncio): Observable<Anuncio> {
    const url = `${this.baseUrl}/anuncio/${anuncio.id}`;
    return this.httpClient.put<Anuncio>(url, anuncio);
  }

  delete(id: string): Observable<Anuncio> {
    const url = `${this.baseUrl}/anuncio/${id}`;
    return this.httpClient.delete<Anuncio>(url);
  }

  get(): Observable<Anuncio[]> {
    return this.httpClient.get<Anuncio[]>(`${this.baseUrl}/anuncio`);
  }

 

}

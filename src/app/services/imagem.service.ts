import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Imagem } from '../models/imagem';

@Injectable({
  providedIn: 'root',
})
export class ImagemService {
  baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) {}

  listarImagens(): Observable<Imagem[]> {
    return this.httpClient.get<Imagem[]>(`${this.baseUrl}/imagem`);
  }

  create(imagem: Imagem): Observable<Imagem> {
    return this.httpClient.post<Imagem>(`${this.baseUrl}/imagem`, imagem);
  }
}

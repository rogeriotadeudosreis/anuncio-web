import { AnuncioDeleteComponent } from './components/anuncio-delete/anuncio-delete.component';
import { AnuncioUpdateComponent } from './components/anuncio-update/anuncio-update.component';
import { DetalhesComponent } from './components/detalhes/detalhes.component';
import { ContatoComponent } from './components/contato/contato.component';
import { AnunciosComponent } from './components/anuncios/anuncios.component';
import { TipoImovelDeleteComponent } from './components/tipo-imovel-delete/tipo-imovel-delete.component';
import { TipoImovelUpdateComponent } from './components/tipo-imovel-update/tipo-imovel-update.component';
import { TipoEmpreendimentoUpdateComponent } from './components/tipo-empreendimento-update/tipo-empreendimento-update.component';
import { CaracteristicaUpdateComponent } from './components/caracteristica-update/caracteristica-update.component';
import { GrupoCaracteristicaDeleteComponent } from './components/grupo-caracteristica-delete/grupo-caracteristica-delete.component';
import { GrupoCaracteristicaUpdateComponent } from './components/grupo-caracteristica-update/grupo-caracteristica-update.component';
import { AllUsuarioComponent } from './components/all-usuario/all-usuario.component';
import { AllTipoImovelComponent } from './components/all-tipo-imovel/all-tipo-imovel.component';
import { AllTipoEmpreendimentoComponent } from './components/all-tipo-empreendimento/all-tipo-empreendimento.component';
import { AllGrupoCaracteristicaComponent } from './components/all-grupo-caracteristica/all-grupo-caracteristica.component';
import { AllCaracteristicaComponent } from './components/all-caracteristica/all-caracteristica.component';
import { ImagemComponent } from './components/imagem/imagem.component';
import { EnderecoComponent } from './components/endereco/endereco.component';
import { DadosComplementaresComponent } from './components/dados-complementares/dados-complementares.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { GrupoCaracteristicaComponent } from './components/grupo-caracteristica/grupo-caracteristica.component';
import { CaracteristicaComponent } from './components/caracteristica/caracteristica.component';
import { TipoEmpreendimentoComponent } from './components/tipo-empreendimento/tipo-empreendimento.component';

import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllAnuncioComponent } from './components/all-anuncio/all-anuncio.component';
import { CreateAnuncioComponent } from './components/create-anuncio/create-anuncio.component';
import { LoginComponent } from './components/login/login.component';

import { TipoImovelComponent } from './components/tipo-imovel/tipo-imovel.component';
import { HomeComponent } from './components/home/home.component';
import { CaracteristicaDeleteComponent } from './components/caracteristica-delete/caracteristica-delete.component';
import { TipoEmpreendimentoDeleteComponent } from './components/tipo-empreendimento-delete/tipo-empreendimento-delete.component';
import { CadastroComponent } from './components/cadastro/cadastro.component';
import { CadastroSucessoComponent } from './components/cadastro-sucesso/cadastro-sucesso.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'anuncios' },

  {
    path: 'all-anuncio',
    component: AllAnuncioComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'create-anuncio',
    component: CreateAnuncioComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'tipo-imovel',
    component: TipoImovelComponent,
  },
  {
    path: 'tipo-empreendimento',
    component: TipoEmpreendimentoComponent,
  },
  {
    path: 'caracteristica',
    component: CaracteristicaComponent,
  },
  {
    path: 'grupo-caracteristica',
    component: GrupoCaracteristicaComponent,
  },
  {
    path: 'usuario',
    component: UsuarioComponent,
  },
  {
    path: 'dados-complementares',
    component: DadosComplementaresComponent,
  },
  { path: 'endereco', component: EnderecoComponent },
  {
    path: 'imagem',
    component: ImagemComponent,
  },
  {
    path: 'all-caracteristica',
    component: AllCaracteristicaComponent,
  },
  {
    path: 'all-grupo-caracteristica',
    component: AllGrupoCaracteristicaComponent,
  },
  {
    path: 'all-tipo-empreendimento',
    component: AllTipoEmpreendimentoComponent,
  },
  {
    path: 'all-tipo-imovel',
    component: AllTipoImovelComponent,
  },
  {
    path: 'all-usuario',
    component: AllUsuarioComponent,
  },
  {
    path: 'update/:id',
    component: GrupoCaracteristicaUpdateComponent,
  },
  {
    path: 'delete/:id',
    component: GrupoCaracteristicaDeleteComponent,
  },
  {
    path: 'caracteristica-update/:id',
    component: CaracteristicaUpdateComponent,
  },
  {
    path: 'caracteristica-delete/:id',
    component: CaracteristicaDeleteComponent,
  },
  {
    path: 'tipo-empreendimento-update/:id',
    component: TipoEmpreendimentoUpdateComponent,
  },
  {
    path: 'tipo-empreendimento-delete/:id',
    component: TipoEmpreendimentoDeleteComponent,
  },
  {
    path: 'tipo-imovel-update/:id',
    component: TipoImovelUpdateComponent,
  },
  {
    path: 'tipo-imovel-delete/:id',
    component: TipoImovelDeleteComponent,
  },
  {
    path: 'anuncios',
    component: AnunciosComponent,
  },
  {
    path: 'contato',
    component: ContatoComponent,
  },
  {
    path: 'cadastro',
    component: CadastroComponent,
  },
  {
    path: 'cadastro-sucesso',
    component: CadastroSucessoComponent,
  },
  {
    path: 'detalhes/:id',
    component: DetalhesComponent,
  },
  {
    path:'anuncio-update/:id',
    component: AnuncioUpdateComponent,
  },
  {
    path: 'anuncio-delete/:id',
    component: AnuncioDeleteComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { CaracteristicaComponent } from './components/caracteristica/caracteristica.component';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateAnuncioComponent } from './components/create-anuncio/create-anuncio.component';
import { AllAnuncioComponent } from './components/all-anuncio/all-anuncio.component';
import { LoginComponent } from './components/login/login.component';
import { PaginanaoencontradaComponent } from './components/paginanaoencontrada/paginanaoencontrada.component';
import { HttpClientModule } from '@angular/common/http';
import { TipoImovelComponent } from './components/tipo-imovel/tipo-imovel.component';
import { HomeComponent } from './components/home/home.component';

import { GrupoCaracteristicaComponent } from './components/grupo-caracteristica/grupo-caracteristica.component';
import { TipoEmpreendimentoComponent } from './components/tipo-empreendimento/tipo-empreendimento.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { DadosComplementaresComponent } from './components/dados-complementares/dados-complementares.component';
import { EnderecoComponent } from './components/endereco/endereco.component';
import { ImagemComponent } from './components/imagem/imagem.component';
import { AllCaracteristicaComponent } from './components/all-caracteristica/all-caracteristica.component';
import { AllGrupoCaracteristicaComponent } from './components/all-grupo-caracteristica/all-grupo-caracteristica.component';
import { AllTipoEmpreendimentoComponent } from './components/all-tipo-empreendimento/all-tipo-empreendimento.component';
import { AllTipoImovelComponent } from './components/all-tipo-imovel/all-tipo-imovel.component';
import { AllUsuarioComponent } from './components/all-usuario/all-usuario.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { registerLocaleData } from '@angular/common';
import { GrupoCaracteristicaUpdateComponent } from './components/grupo-caracteristica-update/grupo-caracteristica-update.component';
import { GrupoCaracteristicaDeleteComponent } from './components/grupo-caracteristica-delete/grupo-caracteristica-delete.component';
import { CaracteristicaUpdateComponent } from './components/caracteristica-update/caracteristica-update.component';

import { NotifierModule } from 'angular-notifier';
import { CaracteristicaDeleteComponent } from './components/caracteristica-delete/caracteristica-delete.component';
import { TipoEmpreendimentoUpdateComponent } from './components/tipo-empreendimento-update/tipo-empreendimento-update.component';
import { TipoEmpreendimentoDeleteComponent } from './components/tipo-empreendimento-delete/tipo-empreendimento-delete.component';
import { TipoImovelUpdateComponent } from './components/tipo-imovel-update/tipo-imovel-update.component';
import { TipoImovelDeleteComponent } from './components/tipo-imovel-delete/tipo-imovel-delete.component';
import { AnunciosComponent } from './components/anuncios/anuncios.component';
import { RouterModule } from '@angular/router';
import { VMessageComponent } from './shared/validator/messages/vmessage.component';
import { VMessageModule } from './shared/validator/messages/vmessage.module';

import localePt from '@angular/common/locales/pt';
import { ContatoComponent } from './components/contato/contato.component';
import { CadastroComponent } from './components/cadastro/cadastro.component';
import { CadastroSucessoComponent } from './components/cadastro-sucesso/cadastro-sucesso.component';
import { DetalhesComponent } from './components/detalhes/detalhes.component';
import { AnuncioUpdateComponent } from './components/anuncio-update/anuncio-update.component';
import { AnuncioDeleteComponent } from './components/anuncio-delete/anuncio-delete.component';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';


registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent,
    CreateAnuncioComponent,
    AllAnuncioComponent,
    LoginComponent,
    PaginanaoencontradaComponent,
    TipoImovelComponent,
    HomeComponent,
    CaracteristicaComponent,
    GrupoCaracteristicaComponent,
    TipoEmpreendimentoComponent,
    UsuarioComponent,
    DadosComplementaresComponent,
    EnderecoComponent,
    ImagemComponent,
    AllCaracteristicaComponent,
    AllGrupoCaracteristicaComponent,
    AllTipoEmpreendimentoComponent,
    AllTipoImovelComponent,
    AllUsuarioComponent,
    GrupoCaracteristicaUpdateComponent,
    GrupoCaracteristicaDeleteComponent,
    CaracteristicaUpdateComponent,
    CaracteristicaDeleteComponent,
    TipoEmpreendimentoUpdateComponent,
    TipoEmpreendimentoDeleteComponent,
    TipoImovelUpdateComponent,
    TipoImovelDeleteComponent,
    AnunciosComponent,
    ContatoComponent,
    CadastroComponent,
    CadastroSucessoComponent,
    DetalhesComponent,
    AnuncioUpdateComponent,
    AnuncioDeleteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    ReactiveFormsModule,
    VMessageModule,
    FontAwesomeModule,
    NotifierModule.withConfig({
      position: {
        horizontal: {
          position: 'right',
        },
        vertical: {
          position: 'top',
        },
      },
    }),
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'pt-BR',
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

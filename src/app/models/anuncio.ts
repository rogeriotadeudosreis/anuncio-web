import { Usuario } from './usuario';
import { TipoImovel } from './tipoImovel';
import { TipoEmpreendimento } from './tipoEmpreendimento';
import { DadosComplementares } from './dadosComplementares';
import { Endereco } from './endereco';
import { Imagem } from './imagem';
import { Caracteristica } from './caracteristica';


export interface Anuncio {
  id?: string;
  categoria: string;
  descricao: string;
  ativo: string;
  dataCadastro: string;
  qtdDeVisualizacoes: string;
  usuario: Usuario;
  tipoImovel: TipoImovel;
  tipoEmpreendimento: TipoEmpreendimento;
  dadosComplementares: DadosComplementares;
  endereco: Endereco;
  caracteristicas: Caracteristica[];
  imagem: Imagem[];
}



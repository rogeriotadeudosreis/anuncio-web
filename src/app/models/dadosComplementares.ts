export interface DadosComplementares {
  id?: number;
  nomeEmpreendimento: string;
  valor: string;
  metrosQuadrados: string;
  qtdQuartos: string;
  qtdSuites: string;
  qtdBanheiros: string;
  qtdVagas: string;
}

import { GrupoCaracteristica } from "./grupoCaracteristica";

export interface Caracteristica {
    id?: string;
    nome: string;
    grupoCaracteristica: GrupoCaracteristica;
  }
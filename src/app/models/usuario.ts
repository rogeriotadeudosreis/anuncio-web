export interface Usuario {
  id?: string;
  nome: string;
  email: string;
  senha: string;
  telefone: string;
  perfil?: string;
  dtCadastro?: '';
  dtAtualizacao?: string;
  ativo?: string;
}
